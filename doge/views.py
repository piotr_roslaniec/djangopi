from django.shortcuts import render
from random import randint

# Create your views here.

doge_array = [
	"https://38.media.tumblr.com/60c52a58d48ca40a8c78c16c80f74094/tumblr_mzo2hftDya1t6uw5ro10_400.gif",
	"https://gs1.wac.edgecastcdn.net/8019B6/data.tumblr.com/a49a8730304a219ec067b38b2a077342/tumblr_muqdtiQmCk1szim6vo1_400.gif",
	"https://img.4plebs.org/boards/s4s/image/1387/05/1387052480205.gif",
]

def random_doge_url():
	doge = randint(0, len(doge_array) - 1)
	return doge_array[doge]

def home(request):
	context = {}
	context['doge_url'] = random_doge_url() 
   	return render(request, 'index.html', context)
